# \[Boost.\]Covariant

Disclaimer: This is currently not an accepted or endorsed Boost Library 

*Alfredo A. Correa* <alfredo.correa@gmail.com>

\[Boost.\]Covariant is a C++ library to facilitate the definition of covariant functions for variant input (`std::variant`).
The library generates covariant functions from callable overloads on multiple variant and non-variant input, effectively achieving convariant return on multiple dispatching.

\[Boost.\]Covariant is not an official Boost library.

\[Boost.\]Covariant depends on Boost.MPL.

The original ideas were discussed in these articles,  [Functions of variants are covariant](https://arne-mertz.de/2018/06/functions-of-variants-are-covariant/) and [Multiple dispatch over covariant functions](https://arne-mertz.de/2019/10/multiple-dispatch-over-covariant-functions/).

# Introduction

Sum types have a range of values that is the sum of the ranges of its parts. `std::variant` is the model representation of *sum types* in C++.

For example `std::variant` can hold an integer value (`int` state) or a double value (`double` state). The use of variant types provides support for polymorphism while maintaining value semantics.

There are only a few intrinsic functions that can be applied directly to an `std::variant` instance in C++; basically, only functions that probe or extract their current type state and value. 
Simple C++ functions over its component states cannot be applied directly to the variant since the type information needs to be probed before calling the corresponding function over the correct type.

Specific C++ functions can be applied through *visitors*. 
However, standard visitors are static and *non-covariant*, stopping polymorphism from propagating through function application.

A basic explanation of variants and their visitors can be found [here](https://arne-mertz.de/2018/05/modern-c-features-stdvariant-and-stdvisit/).

# (Non-covariant) Functions

The free function `std::visit` defines a *protocol* that can be used to apply a corresponding concrete function from a set of overloads or cases. The concrete functions are grouped as a visitor function. A visitor over a variant type is a callable object that is overloaded or can be applied to all the states of the variant.

## Visitor class

The prototypical visitor class has several overloads of `operator()`.

```cpp
struct print_visitor {
  std::ostream& operator()(int a){
    return std::cout << "(int)" << a;
  }
  std::ostream& operator()(double a){
    return std::cout << "(double)" << a;
  }
};

std::variant<double, int> v = 3.14;
std::visit(print_visitor{}, v); // prints "(double)3.14"
```

The overload can include template functions which can exploit common syntax within the variant set.

## On-the-fly visitors

If the function is generic enough and can be used on all the possible variant types, the auxiliary function can be replaced by a single lambda and called on-the-fly:

```cpp
std::visit([](auto const& e)->std::ostream&{return std::cout << e;}, v) << '\n';
```

If a single generic lambda is incapable of handling all the cases, a set of lambdas can be grouped automatically by an overload object. `std::overload` is a proposed addition to the standard library but can be implemented easily:

```cpp
template <class ...Fs>
struct overload : Fs... {
  template <class ...Ts>
  overload(Ts&& ...ts) : Fs{std::forward<Ts>(ts)}...
  {} 
  using Fs::operator()...;
};
template <class ...Ts>
overload(Ts&&...) -> overload<std::remove_reference_t<Ts>...>;
```

Visitors can be applied to multiple arguments as well. 
The only purpose of the `overload` class is to define overloads in the place they are used (on-the-fly), which in turn can become visitor.
A basic description of on-the-fly visitors can be found [here](https://arne-mertz.de/2018/05/overload-build-a-variant-visitor-on-the-fly/).

## Scope and Restrictions of visitors

The free function `std::visit` and the visitor define a function that can be applied over a variant set.

For a visitor to be valid, it needs to overload all the possible cases of the variant, either by overload resolution, template instantiation or implicit type conversion. Each overload is in principle independent (although it probably makes sense that they all implement a common *conceptual* operation) but all the overloads have to return a common type. This *common* return type makes the functions that be applied *non-covariant*.

For example this is not a valid visitor for `std::variant`, because the return type is not common to all cases (`int` and `double`).
```cpp
// invalid visitor, (what type is d?)
auto d = std::visit([](auto n){ return n + 1; }, std::variant<int, double>(3.14));
```

# Co-variant functions

A *covariant* function is one in which the runtime case of the return type varies with the case of the input type(s). 
Covariant functions are a special case of overloads that return a single variant type based on the concrete return types of the overload functions.

The example above is not a valid visitor and therefore it cannot be applied directly to a variant. However, it can be converted into a valid visitor and therefore also a covariant function by *manually* predict the possible states of the result and wrap the result into a common variant.

```cpp
auto next = [](auto n)->std::variant<int, double>{ return n + 1; };
auto d = std::visit(next, std::variant<int, double>(3.14));
assert( std::get<double>(d) = 3.14 + 1.0 );
```

In this case, the possible output types of the overload set are `int` and `double`, therefore the type of `d` is `std::variant`.

The problem with this approach is that it doesn't scale well for more complicated cases; one has to manually keep track of the possible return types of the overload and enforce the variant return type. If the overload is more complicated, it may become harder to account for all the result types. Besides, it is not clear how to handle the special `void` case. For example:

```cpp
auto next_overload = overload(
  [](int          a)->std::variant<int, double, void??>{ return a + 1; },
  [](double       a)->std::variant<int, double, void??>{ return a + 1; },
  [](char          )->std::variant<int, double, void??>{ return int(0); },
  [](std::string   )->std::variant<int, double, void??>{}
)
std::visit(next_overload, v);
```

Note that the possible number of return types of the overload is equal to or smaller than the original variant states. 
If the function takes more than one (variant) argument the number of possible output state multiplies.

Additionally, since `void` is not a regular type, a variant containing a `void` type is invalid. 
Therefore, overloads that return `void` should be handled separately. 
A possible design choice is to map the `void` return to a special `monostate`, which effectively behaves like a "regular `void`".

## Automatic covariant return type deduction

Given an overload set (or callable function) `Overload` and a list of input types `ListInput`, in principle, it is possible to deduce the set of all the possible return types `ResultSet` from all the possible inputs and later construct a variant `Out` for it.

This is, of course, something that can be computed at the time of compilation, for example with a (meta)function `result_set_of`:

```cpp
using Result = results_of_set_t<Overload, ListInput>;
```

where `ListInput` is extracted from a variant type:

```cpp
using ListInput = variant_types_list_t<std::variant<...>>;
```

From the set of results a new variant type can be made:
```cpp
using new_variant = variant_of_set_t<Result>;
```

## Implementation

This type deduction requires a certain degree of metaprogramming. Different techniques can be used to implement the type deduction above.

Here we use the Boost Metaprogramming Library, Boost.MPL. The code is not particularly obvious but is simple from the perspective of *functional* programming:

```cpp
namespace bmp = boost::mpl;

template<class, class> struct variant_push_back;
template<class... Vs, class T>
struct variant_push_back<std::variant<Vs...>, T> {
  using type = std::variant<
    std::conditional_t<
      std::is_same<T, void>::value
      , std::monostate 
      , T
    >,
    Vs...
  >;
};

template<class Set> 
using variant_of_set_t = 
  typename bmp::fold<
    Set,
    std::variant<>,
    variant_push_back<bmp::_1, bmp::_2>
  >::type;
;
```

```cpp
template<class F, class T>
struct apply {
  using type = decltype(std::declval<F>()(std::declval<T>()));
};
template<class O, class Seq> 
using results_of_set_t = 
  typename bmp::fold<
    typename bmp::transform_view<
      Seq,
      apply<O, bmp::_1>
    >::type,
    bmp::set<>,
    bmp::insert<bmp::_1, bmp::_2>
  >::type
;
```

```cpp
template<class T> struct variant_types_list
template<class... Ts>
struct variant_types_list<std::variant<Ts...>> {
  using type = bmp::list<Ts...>;
};
template<class T> using variant_types_list_t = typename variant_types_list<T>::type;
```

Once we are past this metaprogramming interlude, it is relatively straightforward to define a `covariant` wrapper class that generalizes the concept of an overload in order to produce a covariant function. Note that the main complication is to handle the `void` return case.

```cpp
template<class... Fs>
struct covariant : overload<Fs...> {
  covariant(Fs... fs) : overload<Fs...>(fs...) {}
  template<class... Ts, typename = decltype(overload<Fs...>::operator()(std::declval<Ts>()...))> 
  decltype(auto) call(Ts&&... ts) const{
    if constexpr(std::is_same<decltype(overload<Fs...>::operator()(std::forward<Ts>(ts)...)), void>::value) {
      overload<Fs...>::operator()(std::forward<Ts>(ts)...);
      return std::monostate{};
    } else {
      return overload<Fs...>::operator()(std::forward<Ts>(ts)...);
    }
  }
  template<class... Ts, class Ret = variant_of_set_t<detail::results_of_set_t<overload<Fs...> const&, variant_types_list_t<std::variant<Ts...>>>>>
  Ret operator()(std::variant<Ts...> const& v) {
    return std::visit([&](auto&& e)->Ret{ return call(e); }, v);
  }
};

template<class... Fs> covariant(Fs... f) -> covariant<Fs...>;
```

# Result

In the same way that visitors can be applied to variant types and return a single type, a covariant functions can return a new variant type.

This example function gives the next element within the current state of the variant (e.g. `double` or `int`), for a non-numeric input (`std::string`) it returns nothing (`void`) which is turned into an `std::monostate`.

```cpp
std::variant<int, double, std::string> v = 1.2;
auto d = covariant(
  [](int i){ return i + 1; },
  [](double d){ return d + 1; },
  [](auto const&){} // return void otherwise
)(v);
// d is of type std::variant<int, double, std::monostate>
assert( std::get<double>(d) == 1.2 + 1 );
```

For simplicity, the covariance defined here works only with respect to a single argument.
The power of this technique is that it scales to multiple variant arguments at the cost of a slightly more elaborate metaprogramming code. The concept of a covariant function simplifies the propagation of polymorphic values through the use of functions.

The reference implementation can be found in [https://gitlab.com/correaa/boost-covariant](https://gitlab.com/correaa/boost-covariant).

## Failing case, Fallback case.

Just as overloads, all the variant cases have to be handled by the `covariant`.
Some cases can be handled generically, for example by an `auto` case.

Depending on the design, the fallback case can contain error handling code.
At the same time the arguments can be omitted.

```cpp
auto d = covariant(
    [](int    i){return i + 1;},
    [](double d){return d + 1;},
    [](        ){throw std::runtime_error("not implemented";}
)(v);
```

Note that the return type of the fallback case will propagate to the result variant case, for example to `std::monostate`.

# Arity

One of the advantages of `covariant` is that it handles the return deduction automatically. 

Single argument covariant functions, when applied to variant types, can result in the same or other new variant types.
The new resulting variant types of unary covariant function have, at most, the same number of cases as the input variant.

For example, here the cases of the result are variant is unique (`double`):

```cpp
using complex = std::complex<double>;
variant<double, complex> n = 4.; //complex{2.,2.};
variant<double> v2 = covariant([](auto&& n){return std::abs(n);})(n);
```

For n-ary covariant function, instead the possible return types tends to increase faster.
The binary operation over two variants with $N$ cases, can have *at most* $N^2$ cases.

# Non-covariant multiple dispatching

Visitors can be applied to more than one variant and Overloads can have multiple arguments, but as unary visitors it can only have a single return type and therefore this function is not covariant.

```cpp
variant<double, int> v1 = 3.14;
variant<double, int> v2 = 5;

auto d = std::visit(overload{
	[](double d1, double d2){return d1 + d2;        }, 
	[](double d1, int    i2){return d1 + i2;        },// implicit double conversion
	[](int    i1, double d2){return i1 + d2;        },// implicit double conversion
	[](int    i1, int    i2){return double(i1 + i2);} // explicit double conversion
}, v1, v2);
```

Note that the explicit conversion to `double` in the last case is strictly necessary to make the return type uniform (always return `double`).
For the same reason `[](auto x1, auto x2){return x1 + x2;}` is not a valid visitor in this context, but `[](auto x1, auto x2){return double(x1 + x2);}` is.

Generalizing the concept of covariant functions to binary or n-ary functions is not hard; the resulting variant alternatives will contain all the possible output types of the overload evaluated over the product of combinations.
The hardest part is hidden in the implementation because nested loops (combinatorial) cases have to be consider.

## Mixing variadic and non-variant arguments, pivoting visitor

`std::visit` accept multiple variant arguments, however it doesn't allow to mix non-variant arguments.
If we implement `covariant` in terms of `std:visit` we are bound to only use all variant arguments.

```cpp
std::visit(some_visitor, std::variant<double, int>(1.2), 42); // error 42 is not a variant
```

Non-variant arguments can be transformed into variants, but that requires copy and manual coding.

```cpp
std::visit(some_visitor, std::variant<double, int>(1.2), std::variant<int>(42)); // ok, but there is redundant code
```

A better alternative is to create a new visit protocol that accepts non-variants:

```cpp
template<class V, class T, class... Ts>
auto pivot(V&& w, T&& t, Ts&&... ts){
	return pivot(
		[&](auto&&... ts2){return std::forward<V>(w)(std::forward<T>(t), std::forward<decltype(ts2)>(ts2)...);}, 
		std::forward<Ts>(ts)...
	);
}

template<class V, class... Vs, class... Ts>
auto pivot(V&& w, std::variant<Vs...> const& v, Ts&&... ts){
	return visit(
		[&](auto&& vv){return pivot(std::forward<V>(w), std::forward<decltype(vv)>(vv), std::forward<Ts>(ts)...);}, 
		v
	);
}
```

```cpp
pivot(some_visitor, std::variant<double, int>(1.2), 42); // ok
```

`pivot` is a natural generalization of `visit`.

## Combinatorial result types

In the example above, the input types are `{double, int}` for one argument and `{double, int}` the other. Therefore the overload can be applied to the the following four cases `{(double, double), (double, int), (int, double), (int, int)}`.

To achieve this we are going to use a function called `combine_view`. 
`combine_view` takes several sequences of sizes `n1`, `n2`, etc and returns a single sequences of size `n1*n2*etc`. 
For example:

```cpp
using product_set = combine_view<
	boost::mpl::vector<
		boost::mpl::list<double, int>, 
		boost::mpl::list<double, int>
	>
>::type;		
static_assert( boost::mpl::size<product>::value == 4 );
```

Another example showing the combinatorial explosion:

```cpp
using product_set = combine_view<
boost::mpl::vector<
	boost::mpl::list<double, int, std::string>, 
	boost::mpl::list<double, int>,
	boost::mpl::list<std::string, char>
>>::type;		
static_assert( boost::mpl::size<product>::value == 12 );
```

Unfortunately, `combine_view` is not part of Boost.MPL but an [implementation credited to Andrea Rigoni](https://web.archive.org/web/20180919131902/https://stackoverflow.com/questions/1492204/is-it-possible-to-generate-types-with-all-combinations-of-template-arguments/27175631) exist.

Once we have all the combinations, we have to transform it into all the possible output types resulting from the application of the overload.

This is done by generalizing the `apply` metafunction defined above:

```cpp
template<class On, class Args>
struct applyn{
	template<class> struct aux;
	template<std::size_t... Is> struct aux<std::index_sequence<Is...>>{
		using type = decltype(std::declval<On>()(std::declval<typename bmp::at_c<Args, Is>::type>()...));
	};
	using type = typename aux<std::make_index_sequence<bmp::size<Args>::value>>::type;
};
```

`applyn` takes an overload type `On` and a sequence of types (one of the combination of types above) and give back the type of the return.

Finally we can use the `fold`/`transform_view` we applied in the unary covariant version:

```cpp
template<class On, class... Seqs> 
using results_of_setn_t = 
	typename bmp::fold<
		typename bmp::transform_view<
			typename bmp::combine_view<
				bmp::vector<Seqs...>
			>::type
			,
			applyn<On, bmp::_>
		>::type,
		bmp::set<>,
		bmp::insert<bmp::_1, bmp::_2>
	>::type
;
```

Finally we add to our `covariant` wrapper the capability to call with multiple arguments.

```cpp
template<class... Fs>
struct covariant : overload<Fs...>{
	covariant(Fs... fs) : overload<Fs...>(fs...){}
	template<class... Ts, typename = decltype(overload<Fs...>::operator()(std::declval<Ts>()...))> 
	decltype(auto) call(Ts&&... ts) const{
		if constexpr(std::is_same<decltype(overload<Fs...>::operator()(std::forward<Ts>(ts)...)), void>{})
			return overload<Fs...>::operator()(std::forward<Ts>(ts)...), std::monostate{};
		else
			return overload<Fs...>::operator()(std::forward<Ts>(ts)...);
	}
	template<
		class... Variants,
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<Variants>...
			>
		>
	>
	Ret operator()(Variants const&... vs){
		return pivot([&](auto&&... es)->Ret{return call(es...);}, vs...);
	}
};
```

Note that we are using `pivot` as a replacement for `visit`.
At the same time we adopt the convention that `detail::variant_types_list_t<T> = mpl::list<T>` when `T` is not a variant.

# Results

This example, the covariant takes two "numbers", which can be `int`, `double` or `complex` and gives the result in the most obvious set.
Since the function has two arguments and each argument has three cases there are at least 9 overloads.

```cpp
variant<int, double, std::complex<double>> v1 = 3.14;
variant<int, double, std::complex<double>> v2 = std::complex<double>(1.,2.);
auto sum_covariant = covariant(
	[](int i1, int i2){return i1 + i2;}, 
	[](int i1, double d2){return i1 + d2;},
	[](int i1, std::complex<double> c2){return double(i1) + c2;},
	[](double d1, int i2){return d1 + i2;},
	[](double d1, double d2){return d1 + d2;},
	[](double d1, std::complex<double> c2){return d1 + c2;},
	[](std::complex<double> c1, int i2){return c1 + double(i2);},
	[](std::complex<double> c1, double d2){return c1 + d2;},
	[](std::complex<double> c1, std::complex<double> c2){return c1 + c2;}
);
auto result = sum_covariant(v1, v2);
```

This implementation of the `covariant` wrapper allows to convert any overload set in a function that is covariant on multiple arguments where the combinatorial cases are handled by the implementations. However the implementation of the overload set code is still left to the user, which seems to demand the implementation of a combinatorial set functions.
While the combinatorial explosion of return types is handled by this library, the combinatorial explosion of the function overloads is not. Fortunately template functions can help here if we find patterns in the overload set.

# Final example: taming combinatorial overload sets

The idea to overcome this combinatorial explosion is to find common code in the different implementations and delegate it to templates (or lambdas with deduced arguments), effectively combining *generic programming* with runtime dispatching.
Although, variant types don't define any hierarchy of the underlying types, however it is likely that these alternative types fulfill common concepts.

Note above that the code is mostly common (except in the case of integer and complex summation which is not handled by the standard library.)

A first property we can use to simplify the code is to notice that the operation is symmetric.
We can symmetrize the operation and therefore save the body of 3 functions:

```cpp
auto some_sum = overload(
	[](int i1, int i2){return i1 + i2;}, 
	[](int i1, double d2){return i1 + d2;},
	[](int i1, std::complex<double> c2){return double(i1) + c2;},
	[](double d1, double d2){return d1 + d2;},
	[](std::complex<double> c1, double d2){return c1 + d2;},
	[](std::complex<double> c1, std::complex<double> c2){return c1 + c2;}
);	
auto sum_covariant = covariant(
	some_sum, [&](auto t1, auto t2){return partial_sum(t2, t1);}
);
```

Another simplification in the number of overloads involves leaving certain cases undefined (either because certain combinations do not make sense or because the implementation is not yet ready).

```cpp
variant<int, double, std::complex<double>> v1 = 3;
variant<int, double, std::complex<double>> v2 = std::complex<double>(1.,2.);
auto sum_covariant = covariant(
	[](auto n1, auto n2){return n1 + n2;}, 
	[](int, std::complex<double>){throw std::runtime_error{"not"};},
	[](std::complex<double>, int){throw std::runtime_error{"not"};}
);
auto result = sum_covariant(v1, v2);
```

This is were the logical cost of runtime types becomes evident, if some options are left explicitly unimplemented in this way then the code can fail at runtime (just like with interpreted languages like Python). Variant and covariant functions should be used only when true runtime alternatives, e.g. read from a configuration file or runtime user input of type information is provided.

