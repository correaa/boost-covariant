#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && time c++ -std=c++17 -D_TEST_BOOST_COVARIANT_PIVOT $0x.cpp -o $0x && $0x $@ && rm -f $0x.cpp $0x; exit;
#endif

#ifndef BOOST_COVARIANT_PIVOT_HPP
#define BOOST_COVARIANT_PIVOT_HPP

#include<variant>

namespace boost{

template<class Visitor> auto pivot(Visitor&& v){return v();}

template<class V, class T, class... Ts>
auto pivot(V&& w, T&& t, Ts&&... ts){
	return pivot(
		[&](auto&&... ts2){return std::forward<V>(w)(std::forward<T>(t), std::forward<decltype(ts2)>(ts2)...);}, 
		std::forward<Ts>(ts)...
	);
}

template<class V, class... Vs, class... Ts>
auto pivot(V&& w, std::variant<Vs...> const& v, Ts&&... ts){
	return visit(
		[&](auto&& vv){return pivot(std::forward<V>(w), std::forward<decltype(vv)>(vv), std::forward<Ts>(ts)...);}, 
		v
	);
}

template<class V, class... Vs, class... Ts>
auto pivot(V&& w, std::variant<Vs...>& v, Ts&&... ts){
	return visit(
		[&](auto&& vv){return pivot(std::forward<V>(w), std::forward<decltype(vv)>(vv), std::forward<Ts>(ts)...);}, 
		v
	);
}

template<class V, class... Vs, class... Ts>
auto pivot(V&& w, std::variant<Vs...>&& v, Ts&&... ts){
	return visit(
		[&](auto&& vv){return pivot(std::forward<V>(w), std::forward<decltype(vv)>(vv), std::forward<Ts>(ts)...);}, 
		std::move(v)
	);
}

}

#ifdef _TEST_BOOST_COVARIANT_PIVOT

#include<cassert>

struct sum_visitor{
	double operator()(double d, double i) const {return double(d + i);}
	double operator()(double d, int    i) const {return double(d + i);}
	double operator()(int    d, double i) const {return double(d + i);}
	double operator()(int    d, int    i) const {return double(d + i);}
};

int main(){
	using std::variant;

	variant<double, int> v1 = 3.14;
	variant<double, int> v2 = 5;

	auto vret = visit(sum_visitor{}, v1, v2);

	assert(vret == double(std::get<double>(v1) + std::get<int>(v2)));
	std::get<double>(v1) = 3.14;

//	auto vret2 = boost::pivot([a1 = 3.14](auto&& x){return sum_visitor{}(a1, x);}, v2);
//	auto vret2 = boost::pivot( sum_visitor{}, v1, v2 );
//	auto vret2 = boost::pivot( sum_visitor{}, 3.14, v2 );
	auto vv2 = 5;
//	auto vret1 = std::visit( sum_visitor{}, v1, 5 ); assert( vret1 == std::get<double>(v1) + std::get<int>(v2) );
	auto vret1 = boost::pivot(sum_visitor{}, v1, v2 ); //assert( vret1 == std::get<double>(v1) + 5 );

	auto vret2 = boost::pivot(sum_visitor{}, v1, 5  ); assert( vret2 == std::get<double>(v1) + 5 );
	auto vret3 = boost::pivot(sum_visitor{}, 5.1, v2); assert( vret3 == 5.1 + std::get<int>(v2) );
	auto vret4 = boost::pivot(sum_visitor{}, 5.1, 5 ); assert( vret4 == 5.1 + 5 );
}
#endif
#endif

