#ifdef COMPILE
$CXX -std=c++17 -Wfatal-errors $0 -o $0x &&$0x&&rm $0x;exit;
#endif

#ifndef BOOST_SELECT_HPP
#define BOOST_SELECT_HPP

#include<boost/type_index.hpp>

#include<tuple>
#include<variant>

namespace boost{

class bad_variant_selection : public std::runtime_error{
	using std::runtime_error::runtime_error;
};

namespace detail{

#pragma GCC diagnostic push
#if defined(__GNUC__) && (__GNUC__ >= 7)
#pragma GCC diagnostic ignored "-Wunused-but-set-parameter"
#endif
template<std::size_t... Ns , typename... Ts>
auto tail_impl(std::index_sequence<Ns...> , std::tuple<Ts...> t){
	return std::make_tuple(std::get<Ns+1u>(t)...);
}
#pragma GCC diagnostic pop

template<typename... Ts>
auto tail(std::tuple<Ts...> t){
	return tail_impl(std::make_index_sequence<sizeof...(Ts) - 1u>(), t);
}

template<class Ret>
Ret sel_aux(std::tuple<> const&, std::size_t){throw bad_variant_selection{""};}

template<class Ret, class... Ts>
Ret sel_aux(std::tuple<Ts...> const& t, std::size_t s){
	return s?sel_aux<Ret>(tail(t), s-1):std::get<0>(t);
}

template<class Ret>
Ret sel_aux(std::tuple<> const&, std::string const& s){
	throw bad_variant_selection{"couldn't find type name \"" + s + "\" in `" + boost::typeindex::type_id<Ret>().pretty_name() + "`"};
}

template<class Ret, class... Ts>
Ret sel_aux(std::tuple<Ts...> const& t, std::string const& s){
	return 
		(s!=boost::typeindex::type_id<decltype(std::get<0>(t))>().pretty_name())
		?sel_aux<Ret>(tail(t), s)
		:std::get<0>(t)
	;
}

template<class Ret>
Ret sel_aux(std::tuple<> const&, boost::typeindex::stl_type_index const& s){
	throw bad_variant_selection{"couldn't find type name \"" + s.pretty_name() + "\" in `" + boost::typeindex::type_id<Ret>().pretty_name() + "`"};
}

template<class Ret, class... Ts>
Ret sel_aux(std::tuple<Ts...> const& t, boost::typeindex::stl_type_index const& s){
	return 
		(s!=boost::typeindex::type_id<decltype(std::get<0>(t))>())
		?sel_aux<Ret>(tail(t), s)
		:std::get<0>(t)
	;
}

}

template<class... Ts, class T>
auto get(std::tuple<Ts...> const& tp, T const& idx){return detail::sel_aux<std::variant<Ts...>>(tp, idx);}

template<class... Ts>
auto select_from(std::string const& s, std::tuple<Ts...> const& t = {}){
	return detail::sel_aux<std::variant<Ts...>>(t, s);
}

template<class... Ts, class T>
auto one_of(std::string const& s, T&& tt){
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-value"
	return select_from(s, std::tuple<Ts...>{((Ts*)(nullptr), tt)...});
#pragma GCC diagnostic pop
}

template<class... Ts>
auto one_of(std::string const& s){
	return select_from(s, std::tuple<Ts...>{});
}

template<class... Ts>
struct select : std::tuple<Ts...>{
	select() : std::tuple<Ts...>{}{}
	select(Ts... ts) : std::tuple<Ts...>{ts...}{}
	std::variant<Ts...> operator[](std::size_t index) try{
		return detail::sel_aux<std::variant<Ts...>>(*this, index);
	}catch(bad_variant_selection const&){
		throw bad_variant_selection{std::to_string(index)};
	}
	std::variant<Ts...> operator[](std::string const& s){
		return detail::sel_aux<std::variant<Ts...>>(*this, s);
	}
	std::variant<Ts...> operator[](boost::typeindex::stl_type_index const& ti){
		return detail::sel_aux<std::variant<Ts...>>(*this, ti);
	}
};
template<class... Fs> select(Fs... f) -> select<Fs...>;

}

#if not __INCLUDE_LEVEL__ //def _TEST_BOOST_SELECT

#include "../covariant/covariant.hpp"

#include<boost/type_index.hpp>
#include<cassert>
#include<iostream>
#include<variant>
#include<tuple>
#include<complex>

#include<boost/hana/type.hpp>

using std::cout;

int main(){

	using boost::select;
	using std::get;
	using boost::covariant;
	using boost::select_from;
	using boost::one_of;
	using boost::get;

	auto i1 = 0;
	auto v1 = select{3.14, 5, std::string{}}[i1];
	assert( get<double>(v1) == 3.14 );

	auto i2 = 1;
//	auto v2 = select{3.14, 5}[i2];
	auto v2 = get(std::tuple{3.14, 5}, i2);
	assert( get<int>(v2) == 5 );

	{
		assert( get<double>(get(std::tuple{3.14, 5}, "double")) == 3.14 );
		assert( get<double>(get(std::tuple<double, int>{}, "double")) == double{} );
		assert( get<1>(get(std::tuple{3.14, 5}, 1)) == 5 );
	}

	auto i3 = 0;
	auto v3 = select<double, int>{}[i3];
	assert( get<double>(v3) == double{} );

	auto i4 = 1;
	auto v4 = select{double{}, int{}}[i4];
	assert( get<int>(v4) == int{} );

	auto i5 = 1; (void)i5;
	auto v5 = select<double, int>{3.14, {}}[i4]; (void)v5;
	assert( get<int>(v4) == int{} );

	auto i6 = 1;
	auto v6 = select<double, int>{{}, 5}[i6];
	assert( get<int>(v6) == 5 );

	auto i7 = 0; (void)i7;
	auto v7 = select{3.14, 5, std::string{}}["double"];
	assert( get<double>(v7) == 3.14 );

	try{
		auto v8 = select{3.14, 5, std::string{}}["Double"];
		assert( get<double>(v8) == 3.14 );
	}catch(boost::bad_variant_selection const&){}

//	try{
		auto v88 = select_from("double", std::tuple{3.14, 5, std::string{}});
		assert( get<double>(v88) == 3.14 );
//	}catch(boost::bad_variant_selection const&){}

	{
		auto v = one_of<double, int>("double", 10); (void)v;
	}

	auto i9 = boost::typeindex::type_id<double>();
	auto v9 = select{3.14, 5, std::string{}}[i9];
	assert( get<double>(v9) == 3.14 );

	auto& i10 = typeid(double);
	auto v10 = select{3.14, 5, std::string{}}[i10];
	assert( get<double>(v10) == 3.14 );

	for(auto type : {"double", "std::complex<double>"}){
		std::cout << "using " << type << " arithemetic:\n";
		auto number = one_of<double, std::complex<double>>(type, -10.);

		auto number2 = covariant(
			[](auto&& n){
				using std::sqrt; 
				return sqrt(n);
			}
		)(number);
		auto number3 = covariant([](auto&& n){return n*n;})(number2);

		std::visit([&](auto&& e){std::cout << e << '\n';}, number3);
	}

}
#endif
#endif

