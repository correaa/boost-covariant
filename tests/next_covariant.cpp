#ifdef COMPILATION_INSTRUCTIONS
c++ -O3 -std=c++17 -Wall -Wextra -Wpedantic -Wfatal-errors $0 -o $0.x && time $0.x $@ && rm -f $0.x; exit
#endif
//  (C) Copyright Alfredo A. Correa 2018.

#include "../../covariant/covariant.hpp"

#include<cassert>
#include<string>

int main(){

	using boost::covariant;
	auto next_covariant = covariant(
		[](int a){return a + 1;},
		[](double a){return a + 1.0;},
		[](char){return int(-1);},
		[](std::string){return int(-1);}
	);
	
	using std::variant;
	variant<double, int, std::string, char> v = 3.14;
	auto v2 = next_covariant(v);
	
	static_assert(std::is_same<decltype(v2), std::variant<double, int>>{},"!");
	assert( std::get<double>(v2) == 3.14 + 1.0 );
	assert( std::get_if<int>(&v2) == nullptr );

}

