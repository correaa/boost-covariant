#ifdef COMPILATION_INSTRUCTIONS
clang++ -O3 -std=c++17 -Wall -Wextra -Wpedantic -Wfatal-errors $0 -o $0.x && time $0.x $@ && rm -f $0.x; exit
#endif
//  (C) Copyright Alfredo A. Correa 2018.

#include "../../covariant/overload.hpp"

#include<cassert>
#include<string>

int main(){

	using boost::overload;
	auto next_overload = overload(
		[](int a){return a + 1;},
		[](double a){return a + 1;},
		[](char){return int(-1);}
	);

	assert( next_overload(2) == 3 );
	assert( next_overload(3.14) == 3.14 + 1 );
	assert( next_overload('a') == -1 );

	auto next_overload_void = overload(
		[](int a){return a + 1;},
		[](double a){return a + 1;},
		[](char){return int(-1);},
		[](std::string)->void{}
	);
	
	assert( next_overload_void(2) == 3 );
	assert( next_overload_void(3.14) == 3.14 + 1 );
	assert( next_overload_void('a') == -1 );
	assert(( std::is_same<decltype(next_overload_void(std::string{"ss"})), void>{} ));

}

