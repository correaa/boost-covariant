#ifdef compile_instructions
(echo "#include\""$0"\"" > $0.cpp) && c++ -std=c++17 `#-Wfatal-errors` -D_TEST_BOOST_COVARIANT_OVERLOAD $0.cpp -o $0x && $0x $@ && rm $0.cpp $0x; exit;
#endif

#ifndef BOOST_COVARIANT_OVERLOAD_HPP
#define BOOST_COVARIANT_OVERLOAD_HPP

#include<stdexcept>
#include<utility>
#include<variant>

namespace boost{

struct overload_any{template<class T> overload_any(T&&){}};

template<class... Fs> 
struct overload : Fs...{
	using any = overload_any;
	overload(Fs... fs) : Fs(fs)...{}
	using Fs::operator()...;
};
template<class... Fs> overload(Fs... f) -> overload<Fs...>;

template<class... Fs>
class regular_overload{
	overload<Fs...> o_;
public:
	regular_overload(Fs... fs) : o_{fs...}{}
	template<class... Ts, typename V = decltype(o_(std::declval<Ts>()...))>
	decltype(auto) operator()(Ts&&... ts) const{
		if constexpr(std::is_same<V, void>{})
			return o_(std::forward<Ts>(ts)...), std::monostate{};
		else
			return o_(std::forward<Ts>(ts)...);
	}
};
template<class... Fs> regular_overload(Fs... f) -> regular_overload<Fs...>;

template<class... Fs>
class overload_fallback{
	overload<Fs...> o_;
public:
	overload_fallback(Fs... fs) : o_{fs...}{}
	template<class... Ts, typename = decltype(o_(std::declval<Ts>()...))>
	decltype(auto) operator()(Ts&&... ts) const{
		return o_(std::forward<Ts>(ts)...);
	}
	decltype(auto) operator()(...) const{return o_();}
	template<class... Tf, class... Tl>
	auto operator()(Tf&&... tf, overload<>::any, Tl&&...tl) const
	->decltype(operator()(std::forward<Tf>(tf)..., std::forward<Tl>(tl)...)){
		return operator()(std::forward<Tf>(tf)..., std::forward<Tl>(tl)...);
	}
};

template<class... Fs> overload_fallback(Fs... f) -> overload_fallback<Fs...>;

template<class Ret, class... Fs>
struct static_overload : overload<Fs...>{
//	using overload<Fs...>::overload;
	static_overload(Fs... fs) : overload<Fs...>{fs...}{}
	template<class... Ts> Ret operator()(Ts&&... ts){
		return overload<Fs...>::operator()(std::forward<Ts>(ts)...);
	}
};

template<class Ret, class... Fs>
auto make_static_overload(Fs... fs){return static_overload<Ret, Fs...>(fs...);}

#if 0
template<class F>
struct overload<F> : F{
	overload(F f) : F(std::move(f)){};
	using F::operator();
};
template<class F0, class... Fs> 
struct overload<F0, Fs...> : F0, overload<Fs...>{
	overload(F0 f0, Fs... fs) : F0(std::move(f0)), overload<Fs...>(std::move(fs)...){}
	using F0::operator();
	using overload<Fs...>::operator();
};
#endif

}

#ifdef _TEST_BOOST_COVARIANT_OVERLOAD

#include<cassert>
#include<iostream>
#include<variant>
#include<boost/any.hpp>

using std::cout;

int main(){

	using std::variant;
	using boost::overload;
	using boost::overload_fallback;
	using boost::make_static_overload;
	using boost::regular_overload;

	auto next_overload = overload{
		[](int    a){return a + 1;  },
		[](double a){return a + 1;  },
		[](char    ){return int(-1);}
	};
	assert( next_overload(2   ) == 3        );
	assert( next_overload(3.14) == 3.14 + 1 );
	assert( next_overload('a' ) == -1       );
//	assert( next_overload(std::string{}) == -2); // compile error

	auto next_overload_default = overload{
		[](int    a       ){return a + 1;  },
		[](double a       ){return a + 1;  },
		[](char           ){return int(-1);},
		[](overload<>::any){return int{-2};}
	};
	assert( next_overload_default(2   ) == 3          );
	assert( next_overload_default(3.14) == 3.14 + 1   );
	assert( next_overload_default('a' ) == -1         );
	assert( next_overload_default(std::string{}) == -2); // handled by default case

	auto next_overload_fallback = overload_fallback{
		[](int    a  ){return a + 1;  },
		[](double a  ){return a + 1;  },
		[](char      ){return int{-1};},
		[](          ){return int{-2};}
	};
	assert( next_overload_fallback(2   ) == 3          );
	assert( next_overload_fallback(3.14) == 3.14 + 1   );
	assert( next_overload_fallback('a' ) == -1         );
	assert( next_overload_fallback(std::string{}) == -2); // handled by void/empty case

	auto next_overload_fallback_with_throw = boost::overload_fallback{
		[](int    a  ){return a + 1;  },
		[](double a  ){return a + 1;  },
		[](char      ){return int{-1};},
		[](          ){throw std::logic_error{"unimplemented"};}
	};
	assert( next_overload_fallback_with_throw(2   ) == 3          );
	assert( next_overload_fallback_with_throw(3.14) == 3.14 + 1   );
	assert( next_overload_fallback_with_throw('a' ) == -1         );
	try{
		next_overload_fallback_with_throw(std::string{}); // handled by void/empty but throws
	}catch(std::exception& e){std::cout <<"thrown: "<< e.what() << std::endl;}

	variant<double, int> v1 = 3.14;
	variant<double, int> v2 = 5;
	{
		auto o1 = overload{
			[](double d){return 2*d;}, 
			[](int    i){return 2*i;}
		};
		assert( o1(3.14) == 6.28 );
		assert( o1(5) == 10 );

		auto o2 = overload{
			[](auto n){return 2*n;}
		};
		assert( o2(3.14) == 6.28 );
	}
	{
		auto sum_overload = overload{
				[](double d1, double d2)         {return d1 + d2;}, 
				[](double d1, int    i2)         {return d1 + i2;},
				[](int    i1, double d2)         {return i1 + d2;},
				[](int    i1, int    i2)/*->int*/{return i1 + i2;}
		};
		assert( sum_overload(1, 2.1) == 3.1 );
	}
	{
		auto sum_overload = overload{
				[](double d1, double d2)         {return d1 + d2;}, 
				[](double d1, int    i2)         {return d1 + i2;},
				[](int    i1, double d2)         {return i1 + d2;},
				[](overload<>::any, int i)/*->int*/{return -i;}
		};
		assert( sum_overload(std::string("asss"), 2) == -2 );
	//	sum_overload(std::string("asss"), std::string("bla")); // compile error
	}
	{
		auto sum_overload = boost::overload_fallback{
				[](double d1, double d2)         {return d1 + d2;}, 
				[](double d1, int    i2)         {return d1 + i2;},
				[](int    i1, double d2)         {return i1 + d2;},
				[](                    )/*->int*/{return -99;}
		};
		assert( sum_overload(std::string("asss"), 2) == -99 );
	//	sum_overload(std::string("asss"), std::string("bla")); // compile error
	}
	{
		auto o1 = make_static_overload<int>(
			[](double d){return 2*d;}, 
			[](int    i){return 2*i;},
			[](std::string s){return s;} //lazy, only on instantiation will fail
		);
		auto i = o1(5.4);
		static_assert(std::is_same<decltype(i), int>{} , "");
		assert(i == 10);
	}
	{
		auto d = visit(overload{
			[](double d){return 2.*d;        }, 
			[](int    i){return double(2.*i);},
		}, v1);
		assert( d == 6.28 );
	}
	{
	//	auto d = visit(overload{
	//		[](double d1, double d2){return d1 + d2;}, 
	//		[](double d1, int    i2){return d1 + i2;},
	//		[](int    i1, double d2){return i1 + d2;},
	//		[](int    i1, int    i2){return i1 + i2;} // compile error
	//	}, v1, v2);
	}
	{
		auto d = visit(overload{
			[](double d1, double d2){return d1 + d2;        }, 
			[](double d1, int    i2){return d1 + i2;        },
			[](int    i1, double d2){return i1 + d2;        },
			[](int    i1, int    i2){return double(i1 + i2);}
		}, v1, v2);
		assert( d == 8.14 );
	}
	{
		auto r = regular_overload(
			[](double d){return 8.;},
			[](int){}
		)(3);
		static_assert(std::is_same<decltype(r), std::monostate>{} , "");
	}
}
#endif
#endif

