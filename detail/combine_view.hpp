#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp)  && time -f "%es" c++ -std=c++14 -fconcepts `#-Wfatal-errors` -I$HOME/prj -D_TEST_BOOST_MPL_COMBINE -lboost_system -lboost_filesystem $0x.cpp -o $0.x -lstdc++fs -lpthread && $0.x $@ && (xdg-open ${0%.*}.pdf 2>/dev/null &); rm -rf $0x.cpp $0.x
exit
#endif
#ifndef ALF_BOOST_MPL_COMBINE_HPP
#define ALF_BOOST_MPL_COMBINE_HPP

#include<boost/mpl/vector.hpp>
#include<boost/mpl/set.hpp>

#include<boost/mpl/fold.hpp>
#include<boost/mpl/zip_view.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/pop_front.hpp>

namespace boost{
namespace mpl{

template <class Seq, class ItrSeq>
class SequenceCombiner{

template < class _Seq = mpl::vector<int_<1> > >
struct StateSeq{
	typedef typename pop_front<_Seq>::type sequence;
	typedef typename mpl::at<_Seq, int_<0> >::type state;
	typedef _Seq type;
};

template < class _Seq, class _State >
struct set_state{
	typedef StateSeq< typename push_front<_Seq, _State>::type > type;
};

struct NextOp {

template<typename Out, typename In, typename Enable = typename Out::state>
class apply{
	using seq = typename Out::sequence;
	using new_state = typename Out::state;
	using in_seq = typename mpl::at<In,int_<0> >::type;
	using in_itr = typename mpl::at<In,int_<1> >::type;

	using new_seq = typename mpl::push_back<seq, in_itr>::type;
public:
	typedef typename set_state<new_seq, int_<0> >::type type;
};

template<typename Out, typename In>
class apply<Out,In,mpl::int_<1> >{
	typedef typename Out::sequence seq;
	typedef typename Out::state state;
	typedef typename mpl::at<In,int_<0> >::type in_seq;
	typedef typename mpl::at<In,int_<1> >::type in_itr;

	typedef typename mpl::begin<in_seq>::type Itr_begin;
	typedef typename mpl::next<in_itr>::type  Itr_next;
	typedef typename mpl::end<in_seq>::type   Itr_end;

typedef typename mpl::if_<
	boost::is_same<Itr_next,Itr_end>,
	typename mpl::push_back<seq,Itr_begin>::type,
	typename mpl::push_back<seq,Itr_next>::type
>::type new_seq;

typedef typename mpl::if_<boost::is_same<Itr_next,Itr_end>,
	mpl::int_<1>,
	mpl::int_<0>
>::type new_state;

public:
	typedef typename set_state<new_seq, new_state>::type type;

};
};

typedef typename mpl::fold<
	typename mpl::zip_view<mpl::vector<Seq, ItrSeq> >::type,
	StateSeq<>,
	NextOp
>::type StateResult;

public:

typedef typename mpl::if_<
	boost::is_same<typename StateResult::state, int_<1> >,
	typename mpl::transform<Seq, mpl::end<_1> >::type,
	typename StateResult::sequence
>::type next;

};

template<typename Seq, typename Itrs>
struct combine_iterator{
	typedef mpl::forward_iterator_tag category;
	typedef Seq  seq;
	typedef typename transform<Itrs, deref<_1> >::type type;
};

template <class Seq, class Pos>
struct next<typename mpl::combine_iterator<Seq, Pos>>{
    typedef typename mpl::SequenceCombiner<Seq,Pos>::next next_Pos;
    typedef boost::mpl::combine_iterator<Seq, next_Pos> type;
};

template<class Seq>
class combine_view{
	using Pos_begin = typename mpl::transform<Seq, mpl::begin<_1> >::type;
	using Pos_end   = typename mpl::transform<Seq, mpl::end<_1>   >::type;
public:
	using begin = combine_iterator<Seq, Pos_begin>;
	using end   = combine_iterator<Seq, Pos_end>;
	using type  = combine_view;
};

} // mpl
} // boost

#ifdef _TEST_BOOST_MPL_COMBINE

#include "alf/latex/article.hpp"
#include "alf/latex/fullpage.hpp"
#include "alf/latex/echo.hpp"
#include "alf/latex/boost/mpl.hpp"
#include "alf/latex/boost/multi_array.hpp"

#include <boost/mpl/for_each.hpp>
#include <boost/multi_array.hpp>

#include <boost/mpl/equal.hpp>

using namespace latex;

struct PrintSeq{
	article& doc;
	template<class T> void operator()(T const&){doc << T{} << newl;}
};

struct PrintSeq2{
	boost::multi_array<latex::par, 2>& ma_;
	int const i_; 
	int j_;
	template<class T> void operator()(T const&){
		ma_[i_][j_] << typeid(T);
		++j_;
	}
};

struct PrintSeq1{
	boost::multi_array<latex::par, 2>& ma_;
	int i_ = 0;
	template<class T> void operator()(T const&){
		boost::mpl::for_each<T>(PrintSeq2{ma_, i_, 0});
		++i_;
	}
};


int main(){
	article doc{"combine_view.pdf"};

	doc << make_title("Combine") << full_page << no_indent;
	doc << echo_( 1 + 1 );
	using cv = boost::mpl::combine_view<
		boost::mpl::vector<
			boost::mpl::list<double, std::string>, 
			boost::mpl::list<double, int, std::string>
		>
	>::type;
	
    boost::mpl::for_each<cv>( PrintSeq{doc} );

	boost::multi_array<latex::par, 2> ma(boost::extents[10][10]);
	{
	//	int i = 0;
		boost::mpl::for_each<cv>(
			PrintSeq1{ma}
		);
	}
	doc << ma;
	{
		using boost::mpl::combine_view;
		using product = combine_view<
		boost::mpl::list<
			boost::mpl::list<double, int, std::string>, 
			boost::mpl::list<double, int>,
			boost::mpl::list<std::string, char>
		>>::type;		
		static_assert( boost::mpl::size<product>::value == 12 );
	}

	
}
#endif
#endif

