#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time clang++ -std=c++17 `#-Wfatal-errors` -D_TEST_BOOST_COVARIANT_DETAIL_RESULT_OF_SET $0x.cpp -o $0x.cpp.x && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_DETAIL_RESULT_OF_SET_HPP
#define BOOST_COVARIANT_DETAIL_RESULT_OF_SET_HPP

#include<boost/mpl/fold.hpp>
#include<boost/mpl/set.hpp>
#include<boost/mpl/list.hpp>

#include<boost/mpl/transform_view.hpp>

#include"../detail/combine_view.hpp"

#include<variant>

namespace boost{
namespace detail{

namespace bmp = boost::mpl;

template<class F, class T>
struct apply{
	using type = decltype(std::declval<F>()(std::declval<T>()));
};

template<class O, class Seq> 
using results_of_set_t = 
	typename bmp::fold<
		typename bmp::transform_view<
			Seq,
			apply<O, bmp::_1>
		>::type,
		bmp::set<>,
		bmp::insert<bmp::_1, bmp::_2>
	>::type
;

template<class O2, class Pair>
struct apply2{
    using type = decltype(std::declval<O2>()(
            std::declval<typename bmp::at_c<Pair, 0>::type>(), 
            std::declval<typename bmp::at_c<Pair, 1>::type>() 
    ));
};

template<class O2, class Seq1, class... Seqs> 
using results_of_set2_t = 
	typename bmp::fold<
		typename bmp::transform_view<
			typename bmp::combine_view<
				bmp::vector<Seq1, Seqs...>
			>::type
			,
			apply2<O2, bmp::_>
		>::type,
		bmp::set<>,
		bmp::insert<bmp::_1, bmp::_2>
	>::type
;

template<class On, class Args>
struct applyn{
	template<class> struct aux;
	template<std::size_t... Is> struct aux<std::index_sequence<Is...>>{
		using type = decltype(std::declval<On>()(std::declval<typename bmp::at_c<Args, Is>::type>()...));
	};
	using type = typename aux<std::make_index_sequence<bmp::size<Args>::value>>::type;
};

template<class On, class... Seqs> 
using results_of_setn_t = 
	typename bmp::fold<
		typename bmp::transform_view<
			typename bmp::combine_view<
				bmp::vector<Seqs...>
			>::type
			,
			applyn<On, bmp::_>
		>::type,
		bmp::set<>,
		bmp::insert<bmp::_1, bmp::_2>
	>::type
;

}}

#ifdef _TEST_BOOST_COVARIANT_DETAIL_RESULT_OF_SET

#include<boost/mpl/list.hpp>
#include<boost/mpl/size.hpp>

#include<string>

struct F{
	int operator()(int i);//{return i;}
	double operator()(double d);//{return d;}
	double operator()(std::string s);//{return 3.14;}
};

struct F2{
	double operator()(double, double);
	double operator()(double, int);
	std::string operator()(int, double d);
	int operator()(int, int d);
};

int main(){

	namespace bmp = boost::mpl;

	using a_list = bmp::list<double, int, std::string>;
	using a_set = boost::detail::results_of_set_t<F, a_list>;
	static_assert( bmp::size<a_set>{} ==2 , "" );
	
	using a_list1 = bmp::list<double, int>;
	using a_list2 = bmp::list<double, int>;
	using set2 = boost::detail::results_of_set2_t<F2, a_list1, a_list2>;
	static_assert( bmp::size<set2>{} ==3, "" );
	
	using setn = boost::detail::results_of_setn_t<F2, a_list1, a_list2>;
	static_assert( bmp::size<setn>{} ==3, "" );

}
#endif
#endif

