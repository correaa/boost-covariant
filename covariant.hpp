#ifdef COMPILE
$CXX -std=c++17 -Wfatal-errors $0 -o $0x&&$0x&&rm $0x;exit
#endif

#ifndef BOOST_COVARIANT_COVARIANT_HPP
#define BOOST_COVARIANT_COVARIANT_HPP

#include "../covariant/overload.hpp"
#include "../covariant/pivot.hpp"

#include "../covariant/detail/variant_of_set.hpp"
#include "../covariant/detail/result_of_set.hpp"
#include "../covariant/detail/variant_types_list.hpp"

namespace boost{

template<class... Fs>
struct covariant : overload<Fs...>{
	using Base_ = overload<Fs...>;
	covariant(Fs... fs) : Base_(fs...){}
private:
	template<class... Ts, typename Ret = decltype(std::declval<Base_>().operator()(std::declval<Ts>()...))> 
	decltype(auto) call_(Ts&&... ts) const{
		if constexpr(std::is_same<Ret, void>{})
			return Base_::operator()(std::forward<Ts>(ts)...), std::monostate{};
		else
			return Base_::operator()(std::forward<Ts>(ts)...);
	}
#if 0
	template<
		class... Ts, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_set_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<std::variant<Ts...>>
			>
		>
	>
	Ret operator()(std::variant<Ts...> const& v) const{
		return visit([&](auto&& e)->Ret{return call(std::forward<decltype(e)>(e));}, v);
	}
#endif
#if 0
	template<class... T1s, class... T2s, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<std::variant<T1s...>>,
				detail::variant_types_list_t<std::variant<T2s...>>
			>
		>
	>
	Ret operator()(std::variant<T1s...> const& v1, std::variant<T2s...> const& v2) const{
		return visit([&](auto&& e1, auto&& e2)->Ret{return call(std::forward<decltype(e1)>(e1), std::forward<decltype(e2)>(e2));}, v1, v2);
	}
	template<class... T1s, class T2, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<std::variant<T1s...>>,
				detail::variant_types_list_t<T2>
			>
		>
	>
	Ret operator()(std::variant<T1s...> const& v1, T2 const& v2) const{
		return pivot([&](auto&& e1, auto&& e2)->Ret{return call(std::forward<decltype(e1)>(e1), std::forward<decltype(e2)>(e2));}, v1, v2);
	}
	template<class T1, class... T2s, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<T1>,
				detail::variant_types_list_t<std::variant<T2s...>>
			>
		>
	>
	Ret operator()(T1 const& v1, std::variant<T2s...> const& v2) const{
		return pivot([&](auto&& e1, auto&& e2)->Ret{return call(std::forward<decltype(e1)>(e1), std::forward<decltype(e2)>(e2));}, v1, v2);
	}
#endif
public:
	template<
		class... Variants,
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<Variants>...
			>
		>
	>
	Ret operator()(Variants const&... vs) const{
		return pivot([&](auto&&... es)->Ret{return call_(std::forward<decltype(es)>(es)...);}, vs...);
	}
#if 0
	template<class T1, class T2, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<T1>,
				detail::variant_types_list_t<T2>
			>
		>
	>
	Ret operator()(T1 const& v1, T2 const& v2) const{
		return pivot([&](auto&& e1, auto&& e2)->Ret{return call(std::forward<decltype(e1)>(e1), std::forward<decltype(e2)>(e2));}, v1, v2);
	}
	template<class T1, class T2, class T3,
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<T1>,
				detail::variant_types_list_t<T2>,
				detail::variant_types_list_t<T3>
			>
		>
	>
	Ret operator()(T1 const& v1, T2 const& v2, T3 const& v3) const{
		return pivot([&](auto&& e1, auto&& e2, auto&& e3)->Ret{return call(std::forward<decltype(e1)>(e1), std::forward<decltype(e2)>(e2), std::forward<decltype(e3)>(e3));}, v1, v2, v3);
	}
#endif
/*	template<class... T1s, class... T2s, 
		class Ret = detail::variant_of_set_t<
			detail::results_of_setn_t<
				overload<Fs...> const&, 
				detail::variant_types_list_t<std::variant<T1s...>>,
				detail::variant_types_list_t<std::variant<T2s...>>
			>
		>
	>
	Ret operator()(std::variant<T1s...> const& v1, std::variant<T2s...> const& v2){
		return std::visit([&](auto&& e1, auto&& e2)->Ret{return call(e1, e2);}, v1, v2);
	}*/
//	template<
//		class... Variants,
//		class Ret = detail::simplify_variant_t<detail::variant_of_set_t<
//			detail::results_of_setn_t<
//				Base_ const&, detail::variant_types_list_t<Variants>...
//			>
//		>>
//	>
//	Ret operator()(Variants const&... vs){
//		return visito([this](auto&&... es)->Ret{return call(es...);}, vs...);
//	}
};
template<class... Fs> covariant(Fs... f) -> covariant<Fs...>;

}

#if not __INCLUDE_LEVEL__ // def _TEST_BOOST_COVARIANT_COVARIANT

#include<cassert>
#include<iostream>
#include<variant>

#include<boost/variant.hpp>
#include<complex>
#include "../covariant/select.hpp"

#include<boost/any.hpp>

using std::cout;

#include <boost/variant.hpp>
#include <string>
#include <iostream>

struct sum //: public boost::static_visitor<double>
{
	double operator()(double d, double i) const {return double(d + i); }
	double operator()(double d, int i) const {return double(d + i); }
	double operator()(int d, double i) const {return double(d + i); }
	double operator()(int d, int i) const {return double(d + i); }
};

int main(){
#if 0
	{
		std::variant<double, int> v1 = 3.14;
		std::variant<double, int> v2 = 5;
		std::visit([](auto&&... e){return sum{}(1.41, e...);}, v2);
//		visito(sum{}, v1, v2);
	//	std::visit([](auto&&... e){return sum{}(1.41, e...);}, v2);
	}
#endif

	using std::variant;
//	using std::visit;
	using std::is_same;
	using boost::overload;
	using boost::covariant;
	using boost::one_of;
	using boost::select;
	using boost::select_from;

	auto next_overload = overload{
		[](int    a){return a + 1;  },
		[](double a){return a + 1;  },
		[](char    ){return int(-1);}
	};
	assert( next_overload(2   ) == 3        );
	assert( next_overload(3.14) == 3.14 + 1 );
	assert( next_overload('a' ) == -1       );

	variant<double, int, std::string, char> v1 = 3.14;
//	auto r = visit(next_overload, v1); 
//		^^^ not a valid visitor because of incompatible ret types

	auto next_overload_fallback = overload{
		[](int    a  ){return a + 1;  },
		[](double a  ){return a + 1;  },
		[](char      ){return int{-1};},
		[](boost::any){return int{-2};}
	};
	assert( next_overload_fallback(2   ) == 3          );
	assert( next_overload_fallback(3.14) == 3.14 + 1   );
	assert( next_overload_fallback('a' ) == -1         );
	assert( next_overload_fallback(std::string{}) == -2);

	auto next_overload_void = overload{
		[](int          a){return a + 1;  },
		[](double       a){return a + 1;  },
		[](char          ){return int(-1);},
		[](std::string   )->void{}
	};
	static_assert(is_same<decltype(next_overload_void(std::string{})), void>{}); 

	auto next_covariant = covariant{
		[](int          a){return a + 1;  },
		[](double       a){return a + 1.0;},
		[](char          ){return int{-1};},
		[](std::string   ){return int{-2};}
	};
	assert( std::get<int>(next_covariant(2  )) == 3 );
	assert( std::get<double>(next_covariant(4.1)) == 4.1 + 1.0    );
	assert( std::get<int>(next_covariant('a')) == -1           );
	assert( std::get<int>(next_covariant(std::string{})) == -2 );

	auto v2 = next_covariant(v1);
	static_assert(is_same<decltype(v2), variant<double, int>>{});
	assert( std::get<double>(v2) == 3.14 + 1.0 );
	assert( std::get_if<int>(&v2) == nullptr );

	auto next_covariant_void = covariant{
		[](int a){return a + 1;},
		[](double a){return a + 1.0;},
		[](char){return int(-1);},
		[](std::string)->void{}
	};

	assert( std::get<int>(next_covariant_void(2  )) == 3         );
	assert( std::get<double>(next_covariant_void(4.1)) == 4.1 + 1.0 );
	assert( std::get<int>(next_covariant_void('a')) == -1        );
	        std::get<std::monostate>(next_covariant_void(std::string{}));
//	static_assert(is_same<decltype(next_covariant_void(std::string{})), std::>{});

	auto v3 = next_covariant_void(v1);
	static_assert(is_same<decltype(v3), variant<double, int, std::monostate>>{});
	assert( std::get<double>(v3) == 3.14 + 1.0 );
	assert( not std::get_if<int>(&v3) );
	assert( not std::get_if<std::monostate>(&v3) );

	visit(overload{
		[](auto const& v ){std::cout << v;     }, 
		[](std::monostate){std::cout << "mono";}
	}, v3);

	auto next_covariant_fallback = covariant{
		[](int    a){return a + 1;},
		[](double a){return a + 1.0;},
		[](...     ){return int{-1};}
	};
	assert( std::get<int>(next_covariant_fallback(2)) == 3 );
	assert( std::get<double>(next_covariant_fallback(4.1)) == 4.1 + 1.0 );

	{
		variant<int, double> v1 = 3.1;
		variant<int, double> v2 = 11;

		auto sum_covariant = covariant{
			[](auto n1, auto n2){return n1 + n2;},
			[](int n1, int n2){return n1 + n2;}, 
			[](double n1, int n2){return n1 + n2;},
		};

		auto res = sum_covariant(v1, v2);
		assert( std::get<double>(res) == 3.1 + 11);

		auto res2 = sum_covariant(v1, 11);
		assert( std::get<double>(res2) == 3.1 + 11);
//		assert(( std::get<std::complex<double>>(res) == 3.1 + std::complex<double>{1., 2.} ));
//		try{
//			result = sum_covariant(v1, v1);
//		}catch(...){};
//		cout << " index: " << result.index() << '\n';	
	}



#if 0
	auto v5 = next_covariant_fallback(std::variant<int, double, std::string>(std::string("hello")));
	assert( std::get<int>(v5) == -1 );

	{
		using complex = std::complex<double>;
	//	auto n = one_of<double, complex>("double"); //complex{2.,2.};
	//	auto n = select<double, complex>{}["double"];
		auto n = select_from<double, complex>("double");
		variant<double> v2 = covariant([](auto&& n){return std::abs(n);})(n);
		auto d = std::get<0>(v2);
	}
#if 0
#endif
#endif
}
#endif
#endif

